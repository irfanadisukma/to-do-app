part of 'to_do_controller.dart';

@freezed
class ToDoState with _$ToDoState {
   const factory ToDoState({
    required List<ToDoData> toDoList,
    required TextEditingController titleController,
    required bool isEditList
  }) = _ToDoState;

  factory ToDoState.initial() => ToDoState(
    toDoList: dummyData,
    titleController: TextEditingController(),
    isEditList: false
  );
}

var dummyData = [
  ToDoData(title: 'Dummy ToDo 1', finished: false),
  ToDoData(title: 'Dummy ToDo 2', finished: false),
  ToDoData(title: 'Dummy ToDo 3', finished: false)
];