part of 'to_do_controller.dart';

@freezed
class ToDoEvent with _$ToDoEvent {
  const factory ToDoEvent.addToDo({required ToDoData data}) = AddToDo;
  const factory ToDoEvent.onTitleChanged({required String title}) = TitleChanged;
  const factory ToDoEvent.onToDoTap({required int index}) = ToDoTapped;
  const factory ToDoEvent.editToDoList() = EditToDoList;
  const factory ToDoEvent.deleteListItem({required int index}) = DeleteListItem;
}