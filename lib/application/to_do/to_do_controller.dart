import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../../domain/to_do/to_do_data.dart';
import '../../injection.dart';

part 'to_do_controller.freezed.dart';
part 'to_do_event.dart';
part 'to_do_state.dart';

final toDoProvider = StateNotifierProvider.autoDispose<ToDoController, ToDoState>((ref) {
  return getIt<ToDoController>();
});

@injectable
class ToDoController extends StateNotifier<ToDoState> {

  ToDoController() : super(ToDoState.initial());

  Future on(ToDoEvent events) async {
    return events.map(
      addToDo: (e) {
        var tempList = state.toDoList.toList();
        tempList.add(e.data);
        return state = state.copyWith(
          toDoList: tempList
        );
      }, 
      onTitleChanged: (value) {
        return state = state.copyWith(
          titleController: TextEditingController(
            text: value.title
          )
        );
      }, 
      onToDoTap: (ToDoTapped value) {
        var tempList = state.toDoList.toList();
        var tempData = tempList[value.index].copyWith(title: tempList[value.index].title, finished: !tempList[value.index].finished);
        tempList[value.index] = tempData;
        return state = state.copyWith(
          toDoList: tempList
        );
      }, 
      editToDoList: (value) {
        return state = state.copyWith(isEditList: !state.isEditList);
      }, 
      deleteListItem: (value) {
        var tempList = state.toDoList.toList();
        tempList.removeAt(value.index);
        return state = state.copyWith(
          toDoList: tempList
        );
      }, 
    );
  }
}