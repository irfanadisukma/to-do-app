// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'to_do_controller.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ToDoEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ToDoData data) addToDo,
    required TResult Function(String title) onTitleChanged,
    required TResult Function(int index) onToDoTap,
    required TResult Function() editToDoList,
    required TResult Function(int index) deleteListItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ToDoData data)? addToDo,
    TResult? Function(String title)? onTitleChanged,
    TResult? Function(int index)? onToDoTap,
    TResult? Function()? editToDoList,
    TResult? Function(int index)? deleteListItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ToDoData data)? addToDo,
    TResult Function(String title)? onTitleChanged,
    TResult Function(int index)? onToDoTap,
    TResult Function()? editToDoList,
    TResult Function(int index)? deleteListItem,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddToDo value) addToDo,
    required TResult Function(TitleChanged value) onTitleChanged,
    required TResult Function(ToDoTapped value) onToDoTap,
    required TResult Function(EditToDoList value) editToDoList,
    required TResult Function(DeleteListItem value) deleteListItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddToDo value)? addToDo,
    TResult? Function(TitleChanged value)? onTitleChanged,
    TResult? Function(ToDoTapped value)? onToDoTap,
    TResult? Function(EditToDoList value)? editToDoList,
    TResult? Function(DeleteListItem value)? deleteListItem,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddToDo value)? addToDo,
    TResult Function(TitleChanged value)? onTitleChanged,
    TResult Function(ToDoTapped value)? onToDoTap,
    TResult Function(EditToDoList value)? editToDoList,
    TResult Function(DeleteListItem value)? deleteListItem,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ToDoEventCopyWith<$Res> {
  factory $ToDoEventCopyWith(ToDoEvent value, $Res Function(ToDoEvent) then) =
      _$ToDoEventCopyWithImpl<$Res, ToDoEvent>;
}

/// @nodoc
class _$ToDoEventCopyWithImpl<$Res, $Val extends ToDoEvent>
    implements $ToDoEventCopyWith<$Res> {
  _$ToDoEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$AddToDoCopyWith<$Res> {
  factory _$$AddToDoCopyWith(_$AddToDo value, $Res Function(_$AddToDo) then) =
      __$$AddToDoCopyWithImpl<$Res>;
  @useResult
  $Res call({ToDoData data});

  $ToDoDataCopyWith<$Res> get data;
}

/// @nodoc
class __$$AddToDoCopyWithImpl<$Res>
    extends _$ToDoEventCopyWithImpl<$Res, _$AddToDo>
    implements _$$AddToDoCopyWith<$Res> {
  __$$AddToDoCopyWithImpl(_$AddToDo _value, $Res Function(_$AddToDo) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = null,
  }) {
    return _then(_$AddToDo(
      data: null == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as ToDoData,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ToDoDataCopyWith<$Res> get data {
    return $ToDoDataCopyWith<$Res>(_value.data, (value) {
      return _then(_value.copyWith(data: value));
    });
  }
}

/// @nodoc

class _$AddToDo implements AddToDo {
  const _$AddToDo({required this.data});

  @override
  final ToDoData data;

  @override
  String toString() {
    return 'ToDoEvent.addToDo(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddToDo &&
            (identical(other.data, data) || other.data == data));
  }

  @override
  int get hashCode => Object.hash(runtimeType, data);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddToDoCopyWith<_$AddToDo> get copyWith =>
      __$$AddToDoCopyWithImpl<_$AddToDo>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ToDoData data) addToDo,
    required TResult Function(String title) onTitleChanged,
    required TResult Function(int index) onToDoTap,
    required TResult Function() editToDoList,
    required TResult Function(int index) deleteListItem,
  }) {
    return addToDo(data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ToDoData data)? addToDo,
    TResult? Function(String title)? onTitleChanged,
    TResult? Function(int index)? onToDoTap,
    TResult? Function()? editToDoList,
    TResult? Function(int index)? deleteListItem,
  }) {
    return addToDo?.call(data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ToDoData data)? addToDo,
    TResult Function(String title)? onTitleChanged,
    TResult Function(int index)? onToDoTap,
    TResult Function()? editToDoList,
    TResult Function(int index)? deleteListItem,
    required TResult orElse(),
  }) {
    if (addToDo != null) {
      return addToDo(data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddToDo value) addToDo,
    required TResult Function(TitleChanged value) onTitleChanged,
    required TResult Function(ToDoTapped value) onToDoTap,
    required TResult Function(EditToDoList value) editToDoList,
    required TResult Function(DeleteListItem value) deleteListItem,
  }) {
    return addToDo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddToDo value)? addToDo,
    TResult? Function(TitleChanged value)? onTitleChanged,
    TResult? Function(ToDoTapped value)? onToDoTap,
    TResult? Function(EditToDoList value)? editToDoList,
    TResult? Function(DeleteListItem value)? deleteListItem,
  }) {
    return addToDo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddToDo value)? addToDo,
    TResult Function(TitleChanged value)? onTitleChanged,
    TResult Function(ToDoTapped value)? onToDoTap,
    TResult Function(EditToDoList value)? editToDoList,
    TResult Function(DeleteListItem value)? deleteListItem,
    required TResult orElse(),
  }) {
    if (addToDo != null) {
      return addToDo(this);
    }
    return orElse();
  }
}

abstract class AddToDo implements ToDoEvent {
  const factory AddToDo({required final ToDoData data}) = _$AddToDo;

  ToDoData get data;
  @JsonKey(ignore: true)
  _$$AddToDoCopyWith<_$AddToDo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$TitleChangedCopyWith<$Res> {
  factory _$$TitleChangedCopyWith(
          _$TitleChanged value, $Res Function(_$TitleChanged) then) =
      __$$TitleChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String title});
}

/// @nodoc
class __$$TitleChangedCopyWithImpl<$Res>
    extends _$ToDoEventCopyWithImpl<$Res, _$TitleChanged>
    implements _$$TitleChangedCopyWith<$Res> {
  __$$TitleChangedCopyWithImpl(
      _$TitleChanged _value, $Res Function(_$TitleChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
  }) {
    return _then(_$TitleChanged(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$TitleChanged implements TitleChanged {
  const _$TitleChanged({required this.title});

  @override
  final String title;

  @override
  String toString() {
    return 'ToDoEvent.onTitleChanged(title: $title)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TitleChanged &&
            (identical(other.title, title) || other.title == title));
  }

  @override
  int get hashCode => Object.hash(runtimeType, title);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TitleChangedCopyWith<_$TitleChanged> get copyWith =>
      __$$TitleChangedCopyWithImpl<_$TitleChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ToDoData data) addToDo,
    required TResult Function(String title) onTitleChanged,
    required TResult Function(int index) onToDoTap,
    required TResult Function() editToDoList,
    required TResult Function(int index) deleteListItem,
  }) {
    return onTitleChanged(title);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ToDoData data)? addToDo,
    TResult? Function(String title)? onTitleChanged,
    TResult? Function(int index)? onToDoTap,
    TResult? Function()? editToDoList,
    TResult? Function(int index)? deleteListItem,
  }) {
    return onTitleChanged?.call(title);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ToDoData data)? addToDo,
    TResult Function(String title)? onTitleChanged,
    TResult Function(int index)? onToDoTap,
    TResult Function()? editToDoList,
    TResult Function(int index)? deleteListItem,
    required TResult orElse(),
  }) {
    if (onTitleChanged != null) {
      return onTitleChanged(title);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddToDo value) addToDo,
    required TResult Function(TitleChanged value) onTitleChanged,
    required TResult Function(ToDoTapped value) onToDoTap,
    required TResult Function(EditToDoList value) editToDoList,
    required TResult Function(DeleteListItem value) deleteListItem,
  }) {
    return onTitleChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddToDo value)? addToDo,
    TResult? Function(TitleChanged value)? onTitleChanged,
    TResult? Function(ToDoTapped value)? onToDoTap,
    TResult? Function(EditToDoList value)? editToDoList,
    TResult? Function(DeleteListItem value)? deleteListItem,
  }) {
    return onTitleChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddToDo value)? addToDo,
    TResult Function(TitleChanged value)? onTitleChanged,
    TResult Function(ToDoTapped value)? onToDoTap,
    TResult Function(EditToDoList value)? editToDoList,
    TResult Function(DeleteListItem value)? deleteListItem,
    required TResult orElse(),
  }) {
    if (onTitleChanged != null) {
      return onTitleChanged(this);
    }
    return orElse();
  }
}

abstract class TitleChanged implements ToDoEvent {
  const factory TitleChanged({required final String title}) = _$TitleChanged;

  String get title;
  @JsonKey(ignore: true)
  _$$TitleChangedCopyWith<_$TitleChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ToDoTappedCopyWith<$Res> {
  factory _$$ToDoTappedCopyWith(
          _$ToDoTapped value, $Res Function(_$ToDoTapped) then) =
      __$$ToDoTappedCopyWithImpl<$Res>;
  @useResult
  $Res call({int index});
}

/// @nodoc
class __$$ToDoTappedCopyWithImpl<$Res>
    extends _$ToDoEventCopyWithImpl<$Res, _$ToDoTapped>
    implements _$$ToDoTappedCopyWith<$Res> {
  __$$ToDoTappedCopyWithImpl(
      _$ToDoTapped _value, $Res Function(_$ToDoTapped) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? index = null,
  }) {
    return _then(_$ToDoTapped(
      index: null == index
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ToDoTapped implements ToDoTapped {
  const _$ToDoTapped({required this.index});

  @override
  final int index;

  @override
  String toString() {
    return 'ToDoEvent.onToDoTap(index: $index)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToDoTapped &&
            (identical(other.index, index) || other.index == index));
  }

  @override
  int get hashCode => Object.hash(runtimeType, index);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ToDoTappedCopyWith<_$ToDoTapped> get copyWith =>
      __$$ToDoTappedCopyWithImpl<_$ToDoTapped>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ToDoData data) addToDo,
    required TResult Function(String title) onTitleChanged,
    required TResult Function(int index) onToDoTap,
    required TResult Function() editToDoList,
    required TResult Function(int index) deleteListItem,
  }) {
    return onToDoTap(index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ToDoData data)? addToDo,
    TResult? Function(String title)? onTitleChanged,
    TResult? Function(int index)? onToDoTap,
    TResult? Function()? editToDoList,
    TResult? Function(int index)? deleteListItem,
  }) {
    return onToDoTap?.call(index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ToDoData data)? addToDo,
    TResult Function(String title)? onTitleChanged,
    TResult Function(int index)? onToDoTap,
    TResult Function()? editToDoList,
    TResult Function(int index)? deleteListItem,
    required TResult orElse(),
  }) {
    if (onToDoTap != null) {
      return onToDoTap(index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddToDo value) addToDo,
    required TResult Function(TitleChanged value) onTitleChanged,
    required TResult Function(ToDoTapped value) onToDoTap,
    required TResult Function(EditToDoList value) editToDoList,
    required TResult Function(DeleteListItem value) deleteListItem,
  }) {
    return onToDoTap(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddToDo value)? addToDo,
    TResult? Function(TitleChanged value)? onTitleChanged,
    TResult? Function(ToDoTapped value)? onToDoTap,
    TResult? Function(EditToDoList value)? editToDoList,
    TResult? Function(DeleteListItem value)? deleteListItem,
  }) {
    return onToDoTap?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddToDo value)? addToDo,
    TResult Function(TitleChanged value)? onTitleChanged,
    TResult Function(ToDoTapped value)? onToDoTap,
    TResult Function(EditToDoList value)? editToDoList,
    TResult Function(DeleteListItem value)? deleteListItem,
    required TResult orElse(),
  }) {
    if (onToDoTap != null) {
      return onToDoTap(this);
    }
    return orElse();
  }
}

abstract class ToDoTapped implements ToDoEvent {
  const factory ToDoTapped({required final int index}) = _$ToDoTapped;

  int get index;
  @JsonKey(ignore: true)
  _$$ToDoTappedCopyWith<_$ToDoTapped> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EditToDoListCopyWith<$Res> {
  factory _$$EditToDoListCopyWith(
          _$EditToDoList value, $Res Function(_$EditToDoList) then) =
      __$$EditToDoListCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EditToDoListCopyWithImpl<$Res>
    extends _$ToDoEventCopyWithImpl<$Res, _$EditToDoList>
    implements _$$EditToDoListCopyWith<$Res> {
  __$$EditToDoListCopyWithImpl(
      _$EditToDoList _value, $Res Function(_$EditToDoList) _then)
      : super(_value, _then);
}

/// @nodoc

class _$EditToDoList implements EditToDoList {
  const _$EditToDoList();

  @override
  String toString() {
    return 'ToDoEvent.editToDoList()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EditToDoList);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ToDoData data) addToDo,
    required TResult Function(String title) onTitleChanged,
    required TResult Function(int index) onToDoTap,
    required TResult Function() editToDoList,
    required TResult Function(int index) deleteListItem,
  }) {
    return editToDoList();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ToDoData data)? addToDo,
    TResult? Function(String title)? onTitleChanged,
    TResult? Function(int index)? onToDoTap,
    TResult? Function()? editToDoList,
    TResult? Function(int index)? deleteListItem,
  }) {
    return editToDoList?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ToDoData data)? addToDo,
    TResult Function(String title)? onTitleChanged,
    TResult Function(int index)? onToDoTap,
    TResult Function()? editToDoList,
    TResult Function(int index)? deleteListItem,
    required TResult orElse(),
  }) {
    if (editToDoList != null) {
      return editToDoList();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddToDo value) addToDo,
    required TResult Function(TitleChanged value) onTitleChanged,
    required TResult Function(ToDoTapped value) onToDoTap,
    required TResult Function(EditToDoList value) editToDoList,
    required TResult Function(DeleteListItem value) deleteListItem,
  }) {
    return editToDoList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddToDo value)? addToDo,
    TResult? Function(TitleChanged value)? onTitleChanged,
    TResult? Function(ToDoTapped value)? onToDoTap,
    TResult? Function(EditToDoList value)? editToDoList,
    TResult? Function(DeleteListItem value)? deleteListItem,
  }) {
    return editToDoList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddToDo value)? addToDo,
    TResult Function(TitleChanged value)? onTitleChanged,
    TResult Function(ToDoTapped value)? onToDoTap,
    TResult Function(EditToDoList value)? editToDoList,
    TResult Function(DeleteListItem value)? deleteListItem,
    required TResult orElse(),
  }) {
    if (editToDoList != null) {
      return editToDoList(this);
    }
    return orElse();
  }
}

abstract class EditToDoList implements ToDoEvent {
  const factory EditToDoList() = _$EditToDoList;
}

/// @nodoc
abstract class _$$DeleteListItemCopyWith<$Res> {
  factory _$$DeleteListItemCopyWith(
          _$DeleteListItem value, $Res Function(_$DeleteListItem) then) =
      __$$DeleteListItemCopyWithImpl<$Res>;
  @useResult
  $Res call({int index});
}

/// @nodoc
class __$$DeleteListItemCopyWithImpl<$Res>
    extends _$ToDoEventCopyWithImpl<$Res, _$DeleteListItem>
    implements _$$DeleteListItemCopyWith<$Res> {
  __$$DeleteListItemCopyWithImpl(
      _$DeleteListItem _value, $Res Function(_$DeleteListItem) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? index = null,
  }) {
    return _then(_$DeleteListItem(
      index: null == index
          ? _value.index
          : index // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$DeleteListItem implements DeleteListItem {
  const _$DeleteListItem({required this.index});

  @override
  final int index;

  @override
  String toString() {
    return 'ToDoEvent.deleteListItem(index: $index)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DeleteListItem &&
            (identical(other.index, index) || other.index == index));
  }

  @override
  int get hashCode => Object.hash(runtimeType, index);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DeleteListItemCopyWith<_$DeleteListItem> get copyWith =>
      __$$DeleteListItemCopyWithImpl<_$DeleteListItem>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ToDoData data) addToDo,
    required TResult Function(String title) onTitleChanged,
    required TResult Function(int index) onToDoTap,
    required TResult Function() editToDoList,
    required TResult Function(int index) deleteListItem,
  }) {
    return deleteListItem(index);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ToDoData data)? addToDo,
    TResult? Function(String title)? onTitleChanged,
    TResult? Function(int index)? onToDoTap,
    TResult? Function()? editToDoList,
    TResult? Function(int index)? deleteListItem,
  }) {
    return deleteListItem?.call(index);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ToDoData data)? addToDo,
    TResult Function(String title)? onTitleChanged,
    TResult Function(int index)? onToDoTap,
    TResult Function()? editToDoList,
    TResult Function(int index)? deleteListItem,
    required TResult orElse(),
  }) {
    if (deleteListItem != null) {
      return deleteListItem(index);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddToDo value) addToDo,
    required TResult Function(TitleChanged value) onTitleChanged,
    required TResult Function(ToDoTapped value) onToDoTap,
    required TResult Function(EditToDoList value) editToDoList,
    required TResult Function(DeleteListItem value) deleteListItem,
  }) {
    return deleteListItem(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(AddToDo value)? addToDo,
    TResult? Function(TitleChanged value)? onTitleChanged,
    TResult? Function(ToDoTapped value)? onToDoTap,
    TResult? Function(EditToDoList value)? editToDoList,
    TResult? Function(DeleteListItem value)? deleteListItem,
  }) {
    return deleteListItem?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddToDo value)? addToDo,
    TResult Function(TitleChanged value)? onTitleChanged,
    TResult Function(ToDoTapped value)? onToDoTap,
    TResult Function(EditToDoList value)? editToDoList,
    TResult Function(DeleteListItem value)? deleteListItem,
    required TResult orElse(),
  }) {
    if (deleteListItem != null) {
      return deleteListItem(this);
    }
    return orElse();
  }
}

abstract class DeleteListItem implements ToDoEvent {
  const factory DeleteListItem({required final int index}) = _$DeleteListItem;

  int get index;
  @JsonKey(ignore: true)
  _$$DeleteListItemCopyWith<_$DeleteListItem> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ToDoState {
  List<ToDoData> get toDoList => throw _privateConstructorUsedError;
  TextEditingController get titleController =>
      throw _privateConstructorUsedError;
  bool get isEditList => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ToDoStateCopyWith<ToDoState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ToDoStateCopyWith<$Res> {
  factory $ToDoStateCopyWith(ToDoState value, $Res Function(ToDoState) then) =
      _$ToDoStateCopyWithImpl<$Res, ToDoState>;
  @useResult
  $Res call(
      {List<ToDoData> toDoList,
      TextEditingController titleController,
      bool isEditList});
}

/// @nodoc
class _$ToDoStateCopyWithImpl<$Res, $Val extends ToDoState>
    implements $ToDoStateCopyWith<$Res> {
  _$ToDoStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? toDoList = null,
    Object? titleController = null,
    Object? isEditList = null,
  }) {
    return _then(_value.copyWith(
      toDoList: null == toDoList
          ? _value.toDoList
          : toDoList // ignore: cast_nullable_to_non_nullable
              as List<ToDoData>,
      titleController: null == titleController
          ? _value.titleController
          : titleController // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
      isEditList: null == isEditList
          ? _value.isEditList
          : isEditList // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ToDoStateCopyWith<$Res> implements $ToDoStateCopyWith<$Res> {
  factory _$$_ToDoStateCopyWith(
          _$_ToDoState value, $Res Function(_$_ToDoState) then) =
      __$$_ToDoStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<ToDoData> toDoList,
      TextEditingController titleController,
      bool isEditList});
}

/// @nodoc
class __$$_ToDoStateCopyWithImpl<$Res>
    extends _$ToDoStateCopyWithImpl<$Res, _$_ToDoState>
    implements _$$_ToDoStateCopyWith<$Res> {
  __$$_ToDoStateCopyWithImpl(
      _$_ToDoState _value, $Res Function(_$_ToDoState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? toDoList = null,
    Object? titleController = null,
    Object? isEditList = null,
  }) {
    return _then(_$_ToDoState(
      toDoList: null == toDoList
          ? _value._toDoList
          : toDoList // ignore: cast_nullable_to_non_nullable
              as List<ToDoData>,
      titleController: null == titleController
          ? _value.titleController
          : titleController // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
      isEditList: null == isEditList
          ? _value.isEditList
          : isEditList // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ToDoState implements _ToDoState {
  const _$_ToDoState(
      {required final List<ToDoData> toDoList,
      required this.titleController,
      required this.isEditList})
      : _toDoList = toDoList;

  final List<ToDoData> _toDoList;
  @override
  List<ToDoData> get toDoList {
    if (_toDoList is EqualUnmodifiableListView) return _toDoList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_toDoList);
  }

  @override
  final TextEditingController titleController;
  @override
  final bool isEditList;

  @override
  String toString() {
    return 'ToDoState(toDoList: $toDoList, titleController: $titleController, isEditList: $isEditList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ToDoState &&
            const DeepCollectionEquality().equals(other._toDoList, _toDoList) &&
            (identical(other.titleController, titleController) ||
                other.titleController == titleController) &&
            (identical(other.isEditList, isEditList) ||
                other.isEditList == isEditList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_toDoList),
      titleController,
      isEditList);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ToDoStateCopyWith<_$_ToDoState> get copyWith =>
      __$$_ToDoStateCopyWithImpl<_$_ToDoState>(this, _$identity);
}

abstract class _ToDoState implements ToDoState {
  const factory _ToDoState(
      {required final List<ToDoData> toDoList,
      required final TextEditingController titleController,
      required final bool isEditList}) = _$_ToDoState;

  @override
  List<ToDoData> get toDoList;
  @override
  TextEditingController get titleController;
  @override
  bool get isEditList;
  @override
  @JsonKey(ignore: true)
  _$$_ToDoStateCopyWith<_$_ToDoState> get copyWith =>
      throw _privateConstructorUsedError;
}
