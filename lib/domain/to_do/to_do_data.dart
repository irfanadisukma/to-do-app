import 'package:freezed_annotation/freezed_annotation.dart';

part 'to_do_data.freezed.dart';

@freezed
class ToDoData with _$ToDoData {
  factory ToDoData({
    required String title,
    required bool finished
  }) = _ToDoData;
}