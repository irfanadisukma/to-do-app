// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'to_do_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ToDoData {
  String get title => throw _privateConstructorUsedError;
  bool get finished => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ToDoDataCopyWith<ToDoData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ToDoDataCopyWith<$Res> {
  factory $ToDoDataCopyWith(ToDoData value, $Res Function(ToDoData) then) =
      _$ToDoDataCopyWithImpl<$Res, ToDoData>;
  @useResult
  $Res call({String title, bool finished});
}

/// @nodoc
class _$ToDoDataCopyWithImpl<$Res, $Val extends ToDoData>
    implements $ToDoDataCopyWith<$Res> {
  _$ToDoDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? finished = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      finished: null == finished
          ? _value.finished
          : finished // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ToDoDataCopyWith<$Res> implements $ToDoDataCopyWith<$Res> {
  factory _$$_ToDoDataCopyWith(
          _$_ToDoData value, $Res Function(_$_ToDoData) then) =
      __$$_ToDoDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title, bool finished});
}

/// @nodoc
class __$$_ToDoDataCopyWithImpl<$Res>
    extends _$ToDoDataCopyWithImpl<$Res, _$_ToDoData>
    implements _$$_ToDoDataCopyWith<$Res> {
  __$$_ToDoDataCopyWithImpl(
      _$_ToDoData _value, $Res Function(_$_ToDoData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? finished = null,
  }) {
    return _then(_$_ToDoData(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      finished: null == finished
          ? _value.finished
          : finished // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ToDoData implements _ToDoData {
  _$_ToDoData({required this.title, required this.finished});

  @override
  final String title;
  @override
  final bool finished;

  @override
  String toString() {
    return 'ToDoData(title: $title, finished: $finished)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ToDoData &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.finished, finished) ||
                other.finished == finished));
  }

  @override
  int get hashCode => Object.hash(runtimeType, title, finished);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ToDoDataCopyWith<_$_ToDoData> get copyWith =>
      __$$_ToDoDataCopyWithImpl<_$_ToDoData>(this, _$identity);
}

abstract class _ToDoData implements ToDoData {
  factory _ToDoData(
      {required final String title,
      required final bool finished}) = _$_ToDoData;

  @override
  String get title;
  @override
  bool get finished;
  @override
  @JsonKey(ignore: true)
  _$$_ToDoDataCopyWith<_$_ToDoData> get copyWith =>
      throw _privateConstructorUsedError;
}
