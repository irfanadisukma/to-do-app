import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo_list_app/presentation/widgets/home/to_do_list.dart';

import '../../../application/to_do/to_do_controller.dart';
import '../../widgets/dialogs/add_to_do_dialog.dart';

class TodoHomePage extends ConsumerStatefulWidget {
  const TodoHomePage({super.key});

  @override
  TodoHomePageState createState() => TodoHomePageState();
}

class TodoHomePageState extends ConsumerState<TodoHomePage> {

  @override
  Widget build(BuildContext context) {
    var states = ref.watch(toDoProvider);
    var events = ref.watch(toDoProvider.notifier);
    return Scaffold(
      appBar: AppBar(
        title: const Text('To Do List'),
        actions: [
          if(states.toDoList.isNotEmpty)
            GestureDetector(
              onTap: (){
              events.on(const ToDoEvent.editToDoList());
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Center(
                  child: Text(
                    states.isEditList ? 'Done' : 'Edit',
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
            )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const [
            ToDoListWidget()
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          showDialog(
            context: context, 
            builder: (dialogContext) {
              return const AddToDoDialog();
            }
          );
        } ,
        tooltip: 'Add Item',
        child: const Icon(Icons.add),
      ), 
    );
  }
}