
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../application/to_do/to_do_controller.dart';
import '../../../domain/to_do/to_do_data.dart';

class AddToDoDialog extends ConsumerWidget {
  const AddToDoDialog({super.key});

  @override
  Widget build(BuildContext context, ref) {
    var events = ref.watch(toDoProvider.notifier);
    var states = ref.watch(toDoProvider);
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      title: const Text('Add a New To-Do List'),
      content: TextFormField(
        cursorColor: Colors.black,
        maxLength: 20,
        onChanged: (value){
          events.on(ToDoEvent.onTitleChanged(title: value));
        },
        decoration: const InputDecoration(
          labelText: 'Title',
          labelStyle: TextStyle(
            color: Colors.black
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
          ),
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            if(states.titleController.text.isEmpty){
              var snackBar = const SnackBar(content: Text('Title cannot be empty'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
              return;
            }
            events.on(ToDoEvent.addToDo(data: ToDoData(title: states.titleController.text, finished: false))).then((value) {
              events.on(const ToDoEvent.onTitleChanged(title: ''));
                Navigator.pop(context);
            });
          },
          child: const Text('Save')
        )
      ],
    );
  }
}