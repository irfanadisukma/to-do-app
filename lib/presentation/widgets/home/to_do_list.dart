import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../application/to_do/to_do_controller.dart';
import 'components/to_do_item.dart';

class ToDoListWidget extends ConsumerWidget {
  const ToDoListWidget({super.key});

  @override
  Widget build(BuildContext context, ref) {
    final states = ref.watch(toDoProvider);
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      itemCount: states.toDoList.length,
      shrinkWrap: true,
      itemBuilder: (context, index) => ToDoItem(
        data: states.toDoList[index], 
        index: index
      )
    );
  }
}