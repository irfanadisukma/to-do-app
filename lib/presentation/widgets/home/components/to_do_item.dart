import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:todo_list_app/domain/to_do/to_do_data.dart';

import '../../../../application/to_do/to_do_controller.dart';
import '../../../utils/image_name.dart';

class ToDoItem extends ConsumerWidget {
  final ToDoData data;
  final int index;
  const ToDoItem({super.key, required this.data, required this.index});

  @override
  Widget build(BuildContext context, ref) {
    final events = ref.watch(toDoProvider.notifier);
    final states = ref.watch(toDoProvider);
    return ListTile(
      onTap: (){
        if(!states.isEditList) events.on(ToDoEvent.onToDoTap(index: index));
      },
      leading: Container(
        height: 42,
        width: 42,
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.blue
        ),
        child: Text(
          getFirstLetter(data.title),
          style: const TextStyle(color: Colors.white),
        ),
      ),
      title: Text(
        data.title,
        style: TextStyle(
          decoration: data.finished ? TextDecoration.lineThrough : null,
        ),
      ),
      trailing: states.isEditList ? 
      InkWell(
        onTap: (){
          events.on(ToDoEvent.deleteListItem(index: index));
        },
        child: Icon(Icons.delete_rounded, color: Colors.red.withOpacity(0.8))
      ) :
      Icon(Icons.check_circle, color: data.finished ? Colors.green : null),
    );
  }
}