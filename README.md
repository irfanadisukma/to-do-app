# todo_list_app
<img src="./simulator_screenshot_8E2AE885-DE23-4885-BBCE-953A47FDF333.png"  width="480">
<img src="./simulator_screenshot_8FEFD8DB-EFE4-4D0E-AB3C-9DA5CDCF4C08.png"  width="480">

### Install Flutter
Follow this link: https://docs.flutter.dev/get-started/install.

This project is using: 

- Flutter version 3.3.0
- Dart version 2.18.0

This project using Domain Driven Design (DDD) Pattern

ref:(https://codewithandrea.com/articles/flutter-app-architecture-domain-model/)

This project uses riverpod as state management which uses the BLoC pattern on the controller or provider layer.
- application
- domain
- presentation
- infrastructure/data

### Install dependencies
```
flutter pub get
flutter pub run build_runner build --delete-conflicting-outputs
```

### Run the app
```
flutter run
```

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
